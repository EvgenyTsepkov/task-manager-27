package ru.tsc.tsepkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.api.repository.IRepository;
import ru.tsc.tsepkov.tm.api.service.IService;
import ru.tsc.tsepkov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.tsepkov.tm.exception.field.IdEmptyException;
import ru.tsc.tsepkov.tm.exception.field.IndexIncorrectException;
import ru.tsc.tsepkov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public M add(@Nullable M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
