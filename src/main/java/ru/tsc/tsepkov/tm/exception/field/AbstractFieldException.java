package ru.tsc.tsepkov.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            @NotNull String message,
            @NotNull Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
